#!/usr/bin/env python3

# "generate_exr.py" -- a wrapper around flownet2-docker to generate
# OpenEXR motion vector files e.g. for use by NukeXs Kronos node.
# Copyright (C) 2018 Martin Schitter
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse, logging, os, sys, tempfile, subprocess, struct
import numpy as np
import OpenEXR, Imath

def argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("images_dir", help="directoy of numbered source images")
    parser.add_argument("exr_dir", help="output location")
    parser.add_argument('-t', '--tmp_dir', help="directory for temporary image lists and .flow files", 
                        default='tmp')
    parser.add_argument('-p', '--preserve', help="preserve temporary files", action='store_true')
    parser.add_argument('-n', '--network', default='FlowNet2-CSS-ft-sd')
    parser.add_argument('-v', '--verbose', action="count", default=0)
    args = parser.parse_args()
    if args.verbose > 1:
        logging.basicConfig(level=logging.DEBUG)
    elif args.verbose == 1:
        logging.basicConfig(level=logging.INFO)
    for path in [args.images_dir, args.exr_dir, args.tmp_dir]:
        if os.path.relpath(path).startswith('..'):
            logging.warning(f'[{path}] only subdirectories are accessible by the flownet-docker container')
    logging.debug(f'command line parser results: {args}')
    return args    

def generate_image_lists(args):
    if not os.path.isdir(args.images_dir):
        logging.fatal(f"images_dir: '{args.images_dir}' does not exist or isn't a directory")
        sys.exit()

    if not os.path.isdir(args.exr_dir):
        try:
            os.mkdir(args.exr_dir)
        except Exception as err:
            logging.fatal(f'exr_dir: {err}')
            sys.exit()

    if not os.path.isdir(args.tmp_dir):
        try:
            os.mkdir(args.tmp_dir)
        except Exception as err:
            logging.fatal(f'tmp_dir: {err}')
            sys.exit()

    file_list = os.listdir(args.images_dir)
    file_list.sort()
    logging.debug(f'file_list: {file_list}')

    write_list('fwd1.txt', file_list[:-1], args.images_dir)
    write_list('fwd2.txt', file_list[1:], args.images_dir)
    write_list('fwd_out.txt', file_list[:-1], args.tmp_dir, suffix='-fwd.flo')

    file_list.reverse()
    write_list('bwd1.txt', file_list[:-1], args.images_dir)
    write_list('bwd2.txt', file_list[1:], args.images_dir)
    write_list('bwd_out.txt', file_list[:-1], args.tmp_dir, suffix='-bwd.flo')

def write_list(name, l, prefix, suffix=''):
    with open(os.path.join(args.tmp_dir, name), 'w') as f:
        if suffix:
            l = map(lambda x: os.path.splitext(x)[0] + suffix, l)
        l = map(lambda x: os.path.join(prefix, x), l)
        f.write('\n'.join(l))

def process_images(args):

    logging.info("processing forward run...")
    subprocess.run(['/bin/sh', './run-network.sh', '-v', '-n', args.network, 
                    os.path.join(args.tmp_dir, 'fwd1.txt'),
                    os.path.join(args.tmp_dir, 'fwd2.txt'),
                    os.path.join(args.tmp_dir, 'fwd_out.txt')])
    logging.info("processing backward run...")
    subprocess.run(['/bin/sh', './run-network.sh', '-v', '-n', args.network, 
                    os.path.join(args.tmp_dir, 'bwd1.txt'),
                    os.path.join(args.tmp_dir, 'bwd2.txt'),
                    os.path.join(args.tmp_dir, 'bwd_out.txt')])
    logging.warning("we still have to write the OpenEXR files!")

def generate_exrs(arg):
    file_list = os.listdir(args.images_dir)
    file_list.sort()
    suffix_list = map(lambda x: os.path.splitext(x)[0], file_list)
    for suffix in suffix_list:

        fwd = bwd = None

        if os.path.isfile(os.path.join(args.tmp_dir, suffix + '-fwd.flo')):
            fwd = read_flow(os.path.join(args.tmp_dir, suffix + '-fwd.flo'))
       
        if os.path.isfile(os.path.join(args.tmp_dir, suffix + '-bwd.flo')):
            bwd = read_flow(os.path.join(args.tmp_dir, suffix + '-bwd.flo'))

        if fwd is None:
            fwd = np.zeros(bwd.shape, np.float32)

        if bwd is None:
            bwd = np.zeros(fwd.shape, np.float32)

        path = os.path.join(args.exr_dir, suffix + '.exr')
        logging.info(f"writing: {path}")

        header = OpenEXR.Header(*fwd.shape[1::-1])
        header['Compression'] = Imath.Compression(Imath.Compression.ZIPS_COMPRESSION)
        header['channels'] = {'backward.u': Imath.Channel(Imath.PixelType(OpenEXR.HALF)),
                              'backward.v': Imath.Channel(Imath.PixelType(OpenEXR.HALF)),
                              'forward.u':  Imath.Channel(Imath.PixelType(OpenEXR.HALF)),
                              'forward.v':  Imath.Channel(Imath.PixelType(OpenEXR.HALF))}
        exr = OpenEXR.OutputFile(path, header)
        exr.writePixels({'backward.u': bwd[:,:,0].astype(np.float16),
                         'backward.v': (bwd[:,:,1] * -1).astype(np.float16),
                         'forward.u':  fwd[:,:,0].astype(np.float16),
                         'forward.v':  (fwd[:,:,1] * -1).astype(np.float16)})
        exr.close()

def read_flow(path):
    """based on: https://github.com/Johswald/flow-code-python 
    and http://vision.middlebury.edu/flow/code/flow-code/README.txt """

    f = open(path,'rb')
    header = f.read(12)
    tag, w, h = struct.unpack('4sii', header)
    logging.debug(f'flow-header: tag: {tag} w/h: {w} {h}')
    assert tag == b'PIEH', 'Flow number %s incorrect. Invalid .flo file' % tag

    data = np.fromfile(f, np.float32, count=2*w*h)
    # Reshape data into 3D array (columns, rows, bands)
    flow = np.resize(data, (int(h), int(w), 2))	
    f.close()
    return flow

def cleanup(args):
    file_list = []
    for name in 'fwd1.txt fwd2.txt fwd_out.txt bwd1.txt bwd2.txt bwd_out.txt'.split():
        path = os.path.join(args.tmp_dir, name)
        if os.path.isfile(path):
            file_list.append(path)
    for path in os.listdir(args.images_dir):
        basename = os.path.splitext(os.path.basename(path))[0]
        file_list += [os.path.join(args.tmp_dir, basename + '-fwd.flo'),
                      os.path.join(args.tmp_dir, basename + '-bwd.flo')]
    for path in file_list:
        if os.path.isfile(path):
            logging.debug(f'cleanup: {path}')
            os.remove(path)
    if not os.listdir(args.tmp_dir):
        os.rmdir(args.tmp_dir)

if __name__ == '__main__':
    args = argument_parser()
    generate_image_lists(args)
    process_images(args)
    generate_exrs(args)
    if not args.preserve:
        cleanup(args)
